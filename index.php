<?php

session_start();
spl_autoload_register(function ($class) {
    include "class/" . $class . ".php";
});

/*
|--------------------------------------------------------------------------
| Helper functions
|------------------------------------------------------------------------
*/
/**
 * Dump variable.
 */
if (!function_exists('d')) {
    function d()
    {
        call_user_func_array('dump', func_get_args());
    }
}
/**
 * Dump variables and die.
 */
if (!function_exists('dd')) {
    function dd()
    {
        call_user_func_array('dump', func_get_args());
        die();
    }
}

$main = new Main();
echo $main->main();
$main->moveFurniture();

echo "============================================= \n";
echo "DÉBUT DE LA PHASE DE TEST\n";
echo "============================================= \n";

$tableChêne =  new Furniture("Table en chêne", 10, 5, 15);
$tableMarbre =  new Furniture("Table en marbre", 10, 5, 15);
$tv =  new Furniture("Télé led", 10, 5, 15);
$chaise =  new Furniture("Chaise", 12, 7, 12);
echo $tableChêne;
echo "--------------------------------------------- \n";

$kitchen =  new Room("Cuisine", 30, 3, 30, 5, [$tableChêne]);
$salon =  new Room("salon", 30, 3, 30, 5, [$tableMarbre, $chaise]);
$salon->addOneFurniture($tv);
echo $kitchen;
print_r($kitchen->getTabFurnitures());
echo "--------------------------------------------- \n";

$building = new Building("13 Rue Martin Luther King", 14280, "Saint-Contest", 5, [$kitchen]);
try {
    $building->addOneRoom($salon);
} catch (Exception $e) {
    echo 'Exception received : ',  $e->getMessage(), "\n";
}
echo $building;

echo "============================================= \n";
echo "FIN DE LA PHASE DE TEST\n";
echo "============================================= \n";