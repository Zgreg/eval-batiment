<?php

interface Measure
{
    public function volume() : int;
    public function surface() : int;
}