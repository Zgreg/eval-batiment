<?php

/**
 * Class Furniture implement Interface Display, Measure
 * @author Greg DILON
 */
class Furniture implements Display, Measure
{
    private $width;
    private $height;
    private $depth;
    private $name;

    /**
     * Construcot of Furniture class
     *
     * @param string $name
     * @param integer $width
     * @param integer $height
     * @param integer $depth
     */
    function __construct(string $name = "Meuble par défaut", int $width, int $height, int $depth) 
    {
        $this->setWidth($width)
            ->setHeight($height)
            ->setDepth($depth)
            ->setName($name)
        ;
    }

    public function __toString() 
    {
        $className = get_class($this);
        
        $txt = "Vous avez instancié la classe '{$className}' avec comme parramètres : \n";
        $txt .= "La largeur : {$this->getWidth()} cm \n";
        $txt .= "La hauteur : {$this->getHeight()} cm \n";
        $txt .= "La profonfdeur : {$this->getDepth()} cm \n";
        $txt .= "Portant le nom de {$this->getName()} \n";
        $txt .= "Son volume est de {$this->volume()} cm³ \n";
        $txt .= "Sa surface est de {$this->surface()} cm² \n";
        
        return $txt; 
    }

    /**
     * Display information about a piece of furniture
     *
     * @return string
     */
    public function display() : string
    {
        return "        1 {$this->getName()} de {$this->volume()} cm³.\n";
    }

    /**
     * Calculates and returns the overall volume of the piece of furniture.
     *
     * @return integer
     */
    public function volume() : int
    {
        return ($this->surface() * $this->getHeight());
    }

    /**
     * Calculates and returns the overall surface area of the furniture
     *
     * @return integer
     */
    public function surface() : int
    {
        return ($this->getWidth() * $this->getDepth());
    }



    // -------------- START GETTER AND SETTER ZONE --------------
    /**
     * Get the value of width
     * @return integer
     */ 
    public function getWidth() : int
    {
        return $this->width;
    }

    /**
     * Set the value of width
     *
     * @param integer $width
     * @return  self
     */ 
    public function setWidth(int $width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * Get the value of height
     * @return integer
     */ 
    public function getHeight() : int
    {
        return $this->height;
    }

    /**
     * Set the value of height
     *
     * @param integer $height
     * @return  self
     */ 
    public function setHeight(int $height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * Get the value of depth
     * @return integer
     */ 
    public function getDepth() : int
    {
        return $this->depth;
    }

    /**
     * Set the value of depth
     *
     * @param integer $depth
     * @return  self
     */ 
    public function setDepth(int $depth)
    {
        $this->depth = $depth;

        return $this;
    }

    /**
     * Get the value of name
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @param string $name
     * @return  self
     */
    public function setName(string $name)
    {
        $this->name = $name;

        return $this;
    }

    // -------------- END GETTER AND SETTER ZONE --------------
}
