<?php

/**
 * Class Room implement Interface Display, Measure
 * @author Greg DILON
 */
class Room implements Display, Measure
{
    private $name;
    private $width;
    private $height;
    private $depth;
    private $nbMaxFurnitures;
    private $NbFurnitures;
    private $tabFurnitures;

    /**
     * Construcot of Room class
     *
     * @param string $name
     * @param integer $width
     * @param integer $height
     * @param integer $depth
     * @param integer $nbMaxFurnitures
     * @param array $tabFurnitures
     */
    function __construct(string $name, int $width, int $height, int $depth, int $nbMaxFurnitures, array $tabFurnitures) 
    {
        try {
            $this->setName($name)
                ->setWidth($width)
                ->setHeight($height)
                ->setDepth($depth)
                ->setNbMaxFurnitures($nbMaxFurnitures)
                ->setTabFurnitures($tabFurnitures)
                ->setNbFurnitures()
            ;
        } catch (Exception $e) {
            echo 'Exception received : ',  $e->getMessage(), "\n";
        }
    }

    public function __toString() 
    {
        $className = get_class($this);
        
        $txt = "Vous avez instancié la classe '{$className}' avec comme parramètres : \n";
        $txt .= "La largeur : {$this->getWidth()} cm \n";
        $txt .= "La hauteur : {$this->getHeight()} cm \n";
        $txt .= "La profonfdeur : {$this->getDepth()} cm \n";
        $txt .= "Portant le nom de {$this->getName()} \n";
        $txt .= "Comportant {$this->getNbFurnitures()} meubles \n";
        $txt .= "Son volume est de {$this->volume()} m³ \n";
        $txt .= "Sa surface est de {$this->surface()} m² \n";
        $txt .= "Les meubles utilisent au total {$this->furnituresSuface()} m² \n";
        $txt .= "Il reste donc {$this->freeSurface()} m² de libre. \n";
        $txt .= "le nombre maximal de meuble est de {$this->getNbMaxFurnitures()} \n";
        
        return $txt; 
    }
    
    /**
     * Display room information
     *
     * @return string
     */
    public function display() : string
    {
        $txt = "    1 {$this->getName()} ";
        $txt .= "de {$this->surface()} m² ";
        $txt .= "A l'intérieure ce trouve:\n";
        foreach ($this->getTabFurnitures() as $furniture) {
            $txt .= $furniture->display();
        }
        return $txt;
    }

    /**
     * Add One Furniture in tabFurnitures array
     *
     * @param Furniture $furniture
     * @return void
     */
    public function addOneFurniture(Furniture $furniture) 
    {
        if ((round($this->freeSurface() - ($furniture->surface()/100))) > 0 && $this->getNbFurnitures() < $this->getNbMaxFurnitures()) {
            $this->tabFurnitures[$furniture->getName()] = $furniture;
            // Updating the number of pieces of furniture
            $this->setNbFurnitures();
        } else {
            throw new Exception("There is not enough room to add this piece of furniture.");
        }
        
    }

    /**
     * Delete One Furniture in tabFurnitures array
     *
     * @param string $furniture
     * @return void
     */
    public function removeOneFurniture(string $furniture)
    {
        unset($this->tabFurnitures[$furniture]);
        // Updating the number of pieces of furniture
        $this->setNbFurnitures();
    }

    /**
     * Calculates and returns the overall volume of the piece of room.
     *
     * @return integer
     */
    public function volume() : int
    {
        return ($this->surface() * $this->getHeight());
    }

    /**
     * Calculates and returns the overall surface area of the room
     *
     * @return integer
     */
    public function surface() : int
    {
        return ($this->getWidth() * $this->getDepth());
    }
    
    /**
     * Returns the total surface used by the furniture
     *
     * @return integer
     */
    public function furnituresSuface() : int
    {
        $tab = $this->getTabFurnitures();
        $TotalSurface = 0;
        foreach ($tab as $furnitures) {
            $TotalSurface = ($TotalSurface + $furnitures->surface());
        }
        // the result is converted into metres and rounded off.
        return round($TotalSurface/100);
    }

    public function freeSurface() : int
    {
        return round(($this->surface() - $this->furnituresSuface()));
    }



    // -------------- START GETTER AND SETTER ZONE --------------

    /**
     * Get the value of width
     * @return integer
     */ 
    public function getWidth() : int
    {
        return $this->width;
    }

    /**
     * Set the value of width
     *
     * @param integer $width
     * @return  self
     */ 
    public function setWidth(int $width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * Get the value of height
     * @return integer
     */ 
    public function getHeight() : int
    {
        return $this->height;
    }

    /**
     * Set the value of height
     *
     * @param integer $height
     * @return  self
     */ 
    public function setHeight(int $height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * Get the value of depth
     * @return integer
     */
    public function getDepth() : int
    {
        return $this->depth;
    }

    /**
     * Set the value of depth
     *
     * @param integer $depth
     * @return  self
     */ 
    public function setDepth(int $depth)
    {
        $this->depth = $depth;

        return $this;
    }

    /**
     * Get the value of name
     * @return string
     */ 
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @param string $name
     * @return  self
     */ 
    public function setName(string $name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of nbMaxFurnitures
     * @return integer
     */ 
    final private function getNbMaxFurnitures() : int
    {
        return $this->nbMaxFurnitures;
    }

    /**
     * Set the value of nbMaxFurnitures
     *
     * @param integer $nbMaxFurnitures
     * @return  self
     */ 
    final private function setNbMaxFurnitures()
    {
        $this->nbMaxFurnitures = 10;

        return $this;
    }

    /**
     * Get the value of NbFurnitures
     * @return integer
     */ 
    public function getNbFurnitures() : int
    {
        return $this->NbFurnitures;
    }

    /**
     * Set the value of NbFurnitures
     *
     * @return  self
     */ 
    public function setNbFurnitures()
    {
        $this->NbFurnitures = count($this->getTabFurnitures());

        return $this;
    }

    /**
     * Get the value of tabFurniture
     * @return array
     */ 
    public function getTabFurnitures() : array
    {
        return $this->tabFurnitures;
    }

    /**
     * Add the furniture to the array <tabFurniture>
     *
     * @param array $tabFurniture
     * @return  self
     */ 
    public function setTabFurnitures(array $tabFurnitures)
    {
        foreach ($tabFurnitures as $furniture) {
            if (gettype($furniture) === "object" && get_class($furniture) === "Furniture") {
                $this->tabFurnitures[$furniture->getName()] = $furniture;
            } else {
                throw new Exception('The instance is not a piece of furniture.');
            }
        }

        return $this;
    }

    // -------------- END GETTER AND SETTER ZONE --------------
}