<?php

/**
 * Class Main for demonstration 
 * @author Greg DILON
 */
class Main  
{
    private $building;

    function __construct(){} //TODO the main() function is considered as a constructor since it has the same name as the class, so I declared an empty constructor.

    public function main()
    {
        $this->building = new Building("13 Rue Martin Luther King", 14280, "Saint-Contest", 5, [
            new Room("Cuisine", 30, 3, 30, 5, [
                new Furniture("Table en chêne", 10, 5, 15),
                new Furniture("chaise1", 43, 96, 58),
                new Furniture("chaise2", 43, 96, 58),
                new Furniture("chaise3", 43, 96, 58),
                new Furniture("chaise4", 43, 96, 58),
            ]),
            new Room("Salon", 30, 3, 30, 10, [
                new Furniture("Table en Marbre", 10, 5, 15),
                new Furniture("chaise5", 43, 96, 58),
                new Furniture("chaise6", 43, 96, 58),
                new Furniture("chaise7", 43, 96, 58),
            ])
        ]);

        return $this->building->display();
    }

    public function moveFurniture()
    {
        $kitchen = $this->building->getTabRooms()["Cuisine"];
        $diningRoom = $this->building->getTabRooms()["Salon"];
        $chaise = $kitchen->getTabFurnitures()["chaise4"];
        try {
            $diningRoom->addOneFurniture($chaise);
            $kitchen->removeOneFurniture($chaise->getName());
        } catch (Exception $e) {
            echo 'Exception received : ',  $e->getMessage(), "\n";
        }

        echo "============================================= \n";
        echo "DÉPLACEMENT DE LA CHAISE 4 DE LA CUISINE AU SALON\n";
        echo $this->building->display();
    }
}
