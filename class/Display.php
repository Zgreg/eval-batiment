<?php

interface Display
{
    public function display() : string;
}