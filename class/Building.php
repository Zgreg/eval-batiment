<?php

/**
 * Class Building implement Interface Display
 * @author Greg DILON
 */
class Building implements Display
{
    private $address;
    private $zipcode;
    private $city;
    private $nbMaxRooms;
    private $nbRooms;
    private $tabRooms;

    /**
     * Construcot of Buildging class
     *
     * @param string $address
     * @param integer $zipcode
     * @param string $city
     * @param integer $nbMaxRooms
     * @param array $tabRooms
     */
    function __construct(string $address, int $zipcode, string $city, int $nbMaxRooms, array $tabRooms)
    {
        
        try {
            $this->setAddress($address)
                ->setCity($city)
                ->setZipcode($zipcode)
                ->setTabRooms($tabRooms)
                ->setNbMaxRooms($nbMaxRooms)
                ->setNbRooms()
            ;
        } catch (Exception $e) {
            echo 'Exception received : ',  $e->getMessage(), "\n";
        }
    }

    public function __toString() 
    {
        $className = get_class($this);
        
        $txt = "Vous avez instancié la classe '{$className}' avec comme parramètres : \n";
        $txt .= "L'addresse : {$this->getAddress()} \n";
        $txt .= "Le code postal : {$this->getZipcode()} \n";
        $txt .= "La ville : {$this->getCity()} \n";
        $txt .= "Accueillant {$this->getNbRooms()} Pièce(s)\n";
        $txt .= "Le nombre de pièce maximum est de {$this->getNbMaxRooms()}\n";
        $txt .= "Il est possible d'ajouter encore {$this->getFreeRoom()} pièces.\n";
        
        return $txt; 
    }

    /**
     * Display building information
     *
     * @return string
     */
    public function display() : string
    {
        $txt = "Le bâtiment à l'adresse \"{$this->getAddress()}, {$this->getZipcode()} {$this->getCity()}\" possédant {$this->getNbRooms()} pièce(s) dont : \n";
        foreach ($this->getTabRooms() as $room) {
            $txt .= $room->display();
        }
        return $txt;
    }

    /**
     * Add One room in tabRooms array
     *
     * @param Room $room
     * @return void
     */
    public function addOneRoom(Room $room) 
    {
        if ($this->getNbRooms() < $this->getNbMaxRooms()) {
            $this->tabRooms[$room->getName()] = $room;
            $this->setNbRooms();
        } else {
            throw new Exception("The number of pieces has reached the maximum");
        }
    }

    /**
     * Returns the number of rooms that can be added.
     *
     * @return integer
     */
    public function getFreeRoom() : int
    {
        return ($this->getNbMaxRooms() - $this->getNbRooms());
    }



    // -------------- START GETTER AND SETTER ZONE --------------
    /**
     * Get the value of address
     * 
     * @return string
     */ 
    public function getAddress() : string
    {
        return $this->address;
    }

    /**
     * Set the value of address
     *
     * @param string $address
     * @return  self
     */ 
    public function setAddress(string $address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get the value of zipcode
     * 
     * @return integer
     */ 
    public function getZipcode() : int
    {
        return $this->zipcode;
    }

    /**
     * Set the value of zipcode
     *
     * @param integer $zipcode
     * @return  self
     */
    public function setZipcode(int $zipcode)
    {
        if (preg_match("~^[0-9]{5}$~", $zipcode)) {
            $this->zipcode = $zipcode;
        } else {
            echo "Faux";
            throw new Exception('The value does not correspond to a zipcode');
        }
        return $this;
    }

    /**
     * Get the value of city
     * 
     * @return string
     */ 
    public function getCity() : string
    {
        return $this->city;
    }

    /**
     * Set the value of city
     *
     * @param string $city
     * @return  self
     */ 
    public function setCity(string $city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get the value of nbRooms
     * 
     * @return integer
     */ 
    public function getNbRooms() : int
    {
        return $this->nbRooms;
    }

    /**
     * Set the value of nbRooms
     *
     * @return  self
     */ 
    public function setNbRooms()
    {
        $this->nbRooms = count($this->getTabRooms());

        return $this;
    }

    /**
     * Get the value of tabRooms
     * 
     * @return array
     */ 
    public function getTabRooms() : array
    {
        return $this->tabRooms;
    }

    /**
     * Set the value of tabRooms
     *
     * @param array $tabRooms
     * 
     * @return self
     */ 
    public function setTabRooms(array $tabRooms)
    {
        foreach ($tabRooms as $rooms) {
            if (gettype($rooms) === "object" && get_class($rooms) === "Room") {
                $this->tabRooms[$rooms->getName()] = $rooms;
            } else {
                throw new Exception('The instance is not a piece of room.');
            }
        }

        return $this;
    }
    
    /**
     * Get the value of nbMaxRooms
     * 
     * @return integer
     */
    final public function getNbMaxRooms() : int
    {
        return $this->nbMaxRooms;
    }

    /**
     * Set the value of nbMaxRooms
     *
     * @param integer $nbMaxRooms
     * @return  self
     */
    final public function setNbMaxRooms(int $nbMaxRooms)
    {
        $this->nbMaxRooms = $nbMaxRooms;

        return $this;
    }

    // -------------- END GETTER AND SETTER ZONE --------------
}
